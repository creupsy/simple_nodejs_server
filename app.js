const express = require('express')
const app = express()
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

var cors = require('cors');

// use it before all route definitions
app.use(cors({origin: '*'}));

app.get('/', (req,res) => {
  res.send('Hello World')
  console.log(res.body)
})

app.get('/health',(req,res) => {
  res.send('I')
})
app.post('/', function (req, res) {
  res.send('POST request to the homepage');
  console.log(req.body)
})

app.listen(8080,() => {
  console.log("Server up and running")
})

